package com.company;

/**
 * Created by AndrewC on 08/05/2017.
 */
public class Clase2 {
    //Atributo estatico de la clase
    private static int nPersonas;
    //Cada instancia incrementa este atributo
    public Clase2() {
        nPersonas++;
    }
    //Metodo estatico que retorna un atributo estatico
    public static int getNPersonas() {
        return nPersonas;
    }
    //MAIN
    public static void main(String[] args) {
        //Se crean instancias
        Clase2 p1 = new Clase2();
        Clase2 p2 = new Clase2();
        Clase2 p3 = new Clase2();
        //Accedemos al metodo estatico para ver el numero
        //de instancias de tipo Persona creadas
        System.out.println(Clase2.getNPersonas());
    }
}

