package com.company;


// Clase abstracta porque contiene una o mas definiciones de metodos abstractos

abstract class Animal {



    // Variable que contendra el nombre del animal

    private String name;



    // setter para definir el nombre del animal

    public void setName(String name) {

        this.name=name;

    }



    // getter para obtener el nombre del animal

    public String getName() {

        return this.name;

    }



    // Definiemos una clase abstracta

    // Creas un patron de diseño que tienes que seguir obligatoriamente

    // en tu jerarquia de herencia

    public abstract String tipoAnimal();



}



class Perro extends Animal {



    public Perro(String name) {

        super.setName(name);

    }



    // Estamos obligados a crear este metodo, ya que heredamos de Animal

    // que es una clase abstracta, por lo que hay que definir todos los

    // metodos abstractos que tenga

    public String tipoAnimal() {

        return "El animal es un perro. Se llama " + super.getName();

    }

}



class Gato extends Animal {



    public Gato(String name) {

        super.setName(name);

    }



    // Estamos obligados a crear este metodo, ya que heredamos de Animal

    // que es una clase abstracta, por lo que hay que definir todos los

    // metodos abstractos que tenga

    public String tipoAnimal() {

        return "El animal es un gato. Se llama " + super.getName();

    }

}