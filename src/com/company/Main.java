package com.company;

public class Main {

    public static void main(String[] args) {
        class Persona {
            //Atributo estatico de la clase
            private int nPersonas;
            //Cada instancia incrementa este atributo
            public Persona() {
                nPersonas++;
            }
            //Metodo estatico que retorna un atributo estatico
            int getNPersonas() {
                return nPersonas;
            }
            //MAIN
            public void main(String[] args) {
                //Se crean instancias
                Persona p1 = new Persona();
                Persona p2 = new Persona();
                Persona p3 = new Persona();
                //Accedemos al metodo estatico para ver el numero
                //de instancias de tipo Persona creadas

            }
        }
    }
}

