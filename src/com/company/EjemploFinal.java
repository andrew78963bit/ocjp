package com.company;

/**
 * Created by AndrewC on 08/05/2017.
 */
final class Cuadrado extends Rectangulo{
    public Cuadrado(int x, int y, double dimension){
        super(x, y, dimension, dimension);
    }
}
